"use strict";

const Clutter = imports.gi.Clutter;
const Meta = imports.gi.Meta;

const Main = imports.ui.main;

let connected = Array();
const _backgroundActors = global.window_group.get_children()[0].get_children();

//// Public methods ////

function enable() {
	for (let i = 0; i < _backgroundActors.length; i++) {
		connected.push(_backgroundActors[i].connect("scroll-event", _onScroll));
	}
}

function disable() {
	for (var i = 0; i < _backgroundActors.length; i++) {
		_backgroundActors[i].disconnect(connected[i]);
	}
	connected.length = 0;
}

//// Private methods ////

function _onScroll(actor, event) {
	const activeWorkspace = global.workspace_manager.get_active_workspace();
	switch (event.get_scroll_direction()) {
		case Clutter.ScrollDirection.UP:
			Main.wm.actionMoveWorkspace(
				activeWorkspace.get_neighbor(Meta.MotionDirection.UP)
			);
			break;
		case Clutter.ScrollDirection.DOWN:
			Main.wm.actionMoveWorkspace(
				activeWorkspace.get_neighbor(Meta.MotionDirection.DOWN)
			);
	}
}
