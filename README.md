# Scroll Workspaces on Void

https://extensions.gnome.org/extension/2836

A GNOME Shell extension.

Allows scrolling through workspaces when scrolling on an empty area in the desktop.
